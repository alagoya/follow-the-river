﻿using System.Web;
using System.Web.Mvc;

namespace Prueba_Follow_the_River
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
